from json import dumps, loads
from models import Employee
from flask import Flask, render_template, request, redirect
import os

cwd = os.getcwd()
app = Flask(__name__)

@app.route('/', methods=['GET'])
def list_employees():
    _employees = Employee.list()
    return render_template('employee_panel.html', _employees=sorted(_employees, key=lambda s: s.name.lower()))

@app.route('/employee', methods=['GET'])
@app.route('/employee/', methods=['GET'])
def get_employees_api():
    return dumps(Employee.list_as_dict(False))

@app.route('/employee/<employee_id>', methods=['GET'])
def get_employee_api(employee_id):
    _employee = Employee.get(employee_id)
    if _employee:
        return dumps(_employee.as_dict())
    else:
        return "{}"

@app.route('/employee/remove', methods=['POST'])
def remove_employee():
    _employee_id = request.form['employee_id']
    _employee = Employee.get(_employee_id)
    if _employee:
        _employee.delete()
    return redirect('/')

@app.route('/employee/remove/<id>', methods=['GET'])
def remove_employee_api(id):
    _employee = Employee.get(id)
    if _employee:
        _employee.delete()
        return "{status:'ok'}"
    else:
        return "{status:'not found'}"

@app.route('/employee/add', methods=['POST'])
def add_employee():
    _employee = Employee(request.form['name'],request.form['email'], request.form['department'])
    if _employee:
        _employee.save()
    return redirect('/')

@app.route('/employee/add/', methods=['GET'])
def add_employee_api():
    try:
        _employee_raw = loads(request.args.get("employee"))
    except:
        return "{status:'nok',message:'Malformed or invalid employee'}"

    if (_employee_raw):
        _employee = Employee(_employee_raw['name'], _employee_raw['email'], _employee_raw['department'])
        _employee.save()
        return dumps(_employee.as_dict())
    else:
        return "{status:'nok',message:'Invalid employee'}"

if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '8000'))
    except ValueError:
        PORT = 8000
    app.debug = True
    app.run(HOST, PORT)
