Employees
-
### Flask based panel and api to manage employees

This project requires Flask and SQLAlchemy modules to run alongside with python3.4+.
To avoid requirement problems, I have created a virtual environment with all requirements installed.
The requirements file is also available for pip installation.

**To run the app using the virtual environment, just use the command ./app.sh.**

> ***Besides the requirements for the challenge, I have created a few extra functionalities:***

> employee addition tool at the very last line of the employee table

> get, add and remove operations trough the API - documented bellow

### API operations
**To add a new employee trough the API**

URL: http://localhost:8000/employee/add/

Args: employee - json object with name, email and department.

Ex.: http://localhost:8000/employee/add/?employee={%22name%22:%22Chimbinha%22,%22email%22:%22chimbinha@luizalabs.com%22,%22department%22:%22finance%22}


**To get an employee trough the API**

URL: http://localhost:8000/employee/\<employee_id\>

Ex.: http://localhost:8000/employee/1


**To delete an employee trough the API**

URL: http://localhost:8000/employee/remove/\<employee_id\>

Ex.: http://localhost:8000/employee/remove/1