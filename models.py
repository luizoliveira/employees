from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
import os

cwd = os.getcwd()
engine = create_engine('sqlite:////{cwd}/employees.db'.format(cwd=cwd))
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

class Employee(Base):
    __tablename__ = 'employees'

    employee_id = Column('employee_id', Integer, primary_key=True)
    name = Column('name', String(250))
    email = Column('email', String(250), )
    department = Column('department', String(250), index=True)

    def __init__(self, name, email, department):
        self.name = name
        self.email = email
        self.department = department

    def save(self):
        session.add(self)
        session.commit()

    def delete(self):
        session.delete(self)
        session.commit()

    # Returns a dict containing the employee, instead of an alchemy object.
    def as_dict(self, show_id=True):
        if show_id:
            return {c.name: getattr(self, c.name) for c in self.__table__.columns}
        else:
            return {c.name: getattr(self, c.name) for c in self.__table__.columns if c.name != 'employee_id'}

    @staticmethod
    def delete_by_id(id):
        employee = session.query(Employee).get(id)
        if employee:
            session.delete(employee)
            return True

    @staticmethod
    def get(employee_id):
        return session.query(Employee).get(employee_id)

    @staticmethod
    def list():
        return session.query(Employee).all()

    # List employee as dicts, instead of alchemy objects.
    @staticmethod
    def list_as_dict(show_id=True):
        return list(e.as_dict(show_id) for e in session.query(Employee).all())